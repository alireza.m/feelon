This gonna be a project called **Industrial Control Platform**.

Any pushes of `.tex` files to `docs/` will be built as PDF.
[Latest built docs can be found here.](https://gitlab.com/alireza.m/feelon/-/jobs/artifacts/master/browse?job=make_docs)
